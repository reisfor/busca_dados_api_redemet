@echo off

:: Definir os parâmetros da consulta
set api_key=
set data_ini=2023010100
set data_fim=2023010123
set localidades=SBBR

:: Inicializar as variáveis iniciais do programa
set page=1
set all_mensagens=
set last_page=
set next_page_url=

:: Iniciando o processo de requisição à API
:loop
:: Fazer a solicitação à API
set url=https://api-redemet.decea.mil.br/mensagens/metar/%localidades%?api_key=%api_key%&data_ini=%data_ini%&data_fim=%data_fim%&page=%page%

if %page% EQU 1 (
    echo Fazendo a primeira requisição
) else (
    echo Fazendo a requisição da página: %page%
)

:: Fazer a requisição e armazenar o resultado em uma variável
for /f "usebackq delims=" %%I in (`curl -s "%url%"`) do set response=%%I

:: Verificar se a solicitação foi bem-sucedida
for /f "tokens=2 delims=: " %%I in ("%response%") do set status=%%I

:: Continuar se a requisição foi feita com sucesso
if "%status%" == "true" (
    :: Verificar os valores de "last_page" e "next_page_url"
    for /f "tokens=2 delims=: " %%I in ('echo %response% ^| jq ".data.last_page"') do set last_page=%%I
    for /f "tokens=2 delims=: " %%I in ('echo %response% ^| jq ".data.current_page"') do set current_page=%%I
    for /f "tokens=2 delims=: " %%I in ('echo %response% ^| jq ".data.next_page_url"') do set next_page_url=%%I

    if %page% EQU 1 (
        echo Sua solicitação terá um total de %last_page% página(s)
    )

    :: Descobrir a quantidade de registros obtidos na resposta
    for /f "tokens=1,* delims=," %%I in ('echo %response% ^| jq -r ".data.data[]"') do (
        set validade_inicial=%%I
        set mensagem=%%J
        set mensagem=!mensagem:"=!
        set all_mensagens=!all_mensagens!!validade_inicial!,!mensagem!^

    )

    :: Verificar se há mais páginas a serem buscadas
    if "%next_page_url%" == "null" (
        goto finish
    ) else if %last_page% == %page% (
        goto finish
    ) else (
        set /a page+=1
        goto loop
    )
) else (
    :: Exibir uma mensagem de erro
    for /f "tokens=2 delims=: " %%I in ('echo %response% ^| jq ".message"') do set message=%%I
    echo Erro na solicitação: %message%
    exit /b 1
)

:finish
:: Nome do arquivo com o resultado
set nome=msg_%date:~-4%%date:~3,2%%date:~0,2%_%time:~0,2%%time:~3,2%%time:~6,2%.txt

:: Obtendo a quantidade de mensagens constantes no resultado
for /f %%I in ('echo %all_mensagens% ^| find /c /v ""') do set quantidade_mensagens=%%I

:: Salvar o resultado em um arquivo de texto
echo %all_mensagens% > %nome%

:: Fim do programa
echo Sua requisição durou %page% página(s).
echo Foram obtidas %quantidade_mensagens% mensagens.
echo O resultado foi armazenado no arquivo: %nome%
