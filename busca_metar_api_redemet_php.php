#!/usr/bin/php
<?php
// Script Desenvolvido pela Assessoria de Transformação Digital do DECEA (ATD)
// Rio de Janeiro, Julho/2023

// Referência - API-REDEMET: Mensagem METAR
// https://ajuda.decea.mil.br/base-de-conhecimento/api-redemet-mensagem-metar/

// Início da contagem de tempo de execução
$tempo_inicial = time();

// Cada solicitação permite buscar aproximadamente 9000 registros.
// Caso receba a mensagem de erro que o número de registros solicitado foi maior que o permitido, 
// reduza o período ou a quantidade de localidades até obter sucesso
// Por exemplo, uma consulta de todo o ano de 2022, para a localidade SBBR (Brasília), resultou em 9017 mensagens.

// Defina os parâmetros da consulta
// api_key - será a sua própria chave da API da REDEMET
$api_key = "";
// data_ini - YYYYMMDDHH - Parâmetros para a data/hora inicial da consulta desejada
$data_ini = "2023010100";
// data_fim - YYYYMMDDHH - Parâmetros para a data/hora final da consulta desejada
// Para consultar mensagens de um dia inteiro, coloque 00 para HH da data_ini e 23 para HH da data_fim
$data_fim = "2023010123";

// localidades - Lista de localidades para realização da consulta. Deverão ser separadas por vírgula ( Ex: SBBR,SBGL)
$localidades = "SBBR";

// Inicializa as variáveis iniciais do programa
$page = 1;
$all_mensagens = "";
$last_page = "";
$next_page_url = "";

// Iniciando o processo de requisição a API
// Faz enquanto for verdadeiro
while (true) {
    // Faça a solicitação à API
    // Define a URL da requisição
    $url = "https://api-redemet.decea.mil.br/mensagens/metar/$localidades?api_key=$api_key&data_ini=$data_ini&data_fim=$data_fim&page=$page";

    if ($page === 1) {
        echo "Fazendo a primeira requisição\n";
    }

    if ($page > 1) {
        echo "Fazendo a requisição da página: $page\n";
    }

    // Faz a requisição e armazena o resultado em uma variável
    $response = file_get_contents($url);

    // Decodifica a resposta JSON
    $response_data = json_decode($response, true);

    // Verifique se a solicitação foi bem-sucedida
    $status = $response_data['status'];

    // Continua caso a requisição tenha sido feita com sucesso
    if ($status === true) {
        // Verificar os valores de "last_page" e "next_page_url"
        $last_page = $response_data['data']['last_page'];
        $current_page = $response_data['data']['current_page'];
        $next_page_url = $response_data['data']['next_page_url'];

        if ($page === 1) {
            echo "Sua solicitação terá um total de $last_page página(s)\n";
        }

        // Descobrindo a quantidade de registros obtidos na resposta
        $mensagens = $response_data['data']['data'];
        $tamanho = count($mensagens);

        for ($i = 0; $i < $tamanho; $i++) {
            $validade_inicial = $mensagens[$i]['validade_inicial'];
            $mens = $mensagens[$i]['mens'];
            $mensagem = "$validade_inicial,$mens";
            $all_mensagens .= $mensagem . "\n";
        }

        // Verificar se há mais páginas a serem buscadas
        if ($next_page_url === null || $last_page === $page) {
            break;
        }

        // Incrementar o número da página
        $page++;
    } else {
        // Exiba uma mensagem de erro
        $message = $response_data['message'];
        echo "Erro na solicitação: $message\n";
        exit(1);
    }
}

// Nome do Arquivo com o resultado
$nome = "msg_" . date("Ymd_His") . ".txt";

// Obtendo a quantidade de mensagens constantes no resultado
$quantidade_mensagens = substr_count($all_mensagens, "\n");

$all_mensagens = explode("\n",$all_mensagens);

sort($all_mensagens);

file_put_contents($nome,implode("\n", $all_mensagens));

// Fim da contagem de tempo de execução
$end_time = time() - $tempo_inicial;
if ($end_time < 60) {
    $duracao = "$end_time segundos";
} else {
    $minutes = floor($end_time / 60);
    $duracao = "$minutes minutos";
}

echo "Sua requisição durou $duracao.\n";
echo "Foram obtidas $quantidade_mensagens mensagens\n";
echo "O resultado foi armazenado no arquivo: $nome\n";
?>
