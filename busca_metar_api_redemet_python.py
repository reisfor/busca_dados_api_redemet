import requests

# Definir os parâmetros da consulta
api_key = ""
data_ini = "2023010100"
data_fim = "2023010123"
localidades = "SBBR"

# Inicializar as variáveis iniciais do programa
page = 1
all_mensagens = ""
last_page = ""
next_page_url = ""

# Iniciando o processo de requisição à API
while True:
    # Fazer a solicitação à API
    url = f"https://api-redemet.decea.mil.br/mensagens/metar/{localidades}?api_key={api_key}&data_ini={data_ini}&data_fim={data_fim}&page={page}"

    if page == 1:
        print("Fazendo a primeira requisição")
    else:
        print(f"Fazendo a requisição da página: {page}")

    # Fazer a requisição e armazenar o resultado em uma variável
    response = requests.get(url).json()

    # Verificar se a solicitação foi bem-sucedida
    status = response.get("status")

    # Continuar se a requisição foi feita com sucesso
    if status:
        # Verificar os valores de "last_page" e "next_page_url"
        last_page = response["data"]["last_page"]
        current_page = response["data"]["current_page"]
        next_page_url = response["data"]["next_page_url"]

        if page == 1:
            print(f"Sua solicitação terá um total de {last_page} página(s)")

        # Descobrir a quantidade de registros obtidos na resposta
        registros = response["data"]["data"]
        tamanho = len(registros)

        for registro in registros:
            validade_inicial = registro["validade_inicial"]
            mensagem = registro["mens"]
            all_mensagens += f"{validade_inicial},{mensagem}\n"

        # Verificar se há mais páginas a serem buscadas
        if not next_page_url or last_page == page:
            break

        # Incrementar o número da página
        page += 1
    else:
        # Exibir uma mensagem de erro
        message = response.get("message")
        print(f"Erro na solicitação: {message}")
        exit(1)

# Nome do arquivo com o resultado
import datetime
nome = f"msg_{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}.txt"

# Obtendo a quantidade de mensagens constantes no resultado
quantidade_mensagens = all_mensagens.count('\n')

# Salvar o resultado em um arquivo de texto
with open(nome, 'w') as file:
    file.write(all_mensagens)

# Fim do programa
print(f"Sua requisição durou {page} página(s).")
print(f"Foram obtidas {quantidade_mensagens} mensagens.")
print(f"O resultado foi armazenado no arquivo: {nome}")
