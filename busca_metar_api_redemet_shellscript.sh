#!/bin/bash
# Script Desenvolvido pela Assessoria de Transformação Digital do DECEA (ATD)
# Rio de Janeiro, Julho/2023

# Referência - API-REDEMET: Mensagem METAR
# https://ajuda.decea.mil.br/base-de-conhecimento/api-redemet-mensagem-metar/

# Início da contagem de tempo de execução
tempo_inicial=$SECONDS

# Cada solicitação permite buscar aproximadamente 9000 registros.
# Caso receba a mensagem de erro que o número de registros solicitado foi maior que o permitido, 
# reduza o período ou a quantidade de localidades até obter sucesso
# Por exemplo, uma consulta de todo o ano de 2022, para a localidade SBBR (Brasília), resultou em 9017 mensagens.

# Defina os parâmetros da consulta
# api_key - será a sua propria chave da API da REDEMET
api_key=""
# data_ini - YYYYMMDDHH - Parâmetros para a data/hora inicial da consulta desejada
data_ini="2023010100"
# data_fim - YYYYMMDDHH - Parâmetros para a data/hora final da consulta desejada
# Para consultar mensagens de um dia inteiro, coloque 00 para HH da data_ini e 23 para HH da data_fim
data_fim="2023010123"
# localidades - Lista de localidades para realização da consulta. Deverao ser separadas por vírgula ( Ex: SBBR,SBGL)
localidades="SBBR"

# Inicializa as variáveis iniciais do programa
page=1
all_mensagens=""
last_page=""
next_page_url=""

# Iniciando o processo de requisição a API
# Faz enquanto for verdadeiro
while true; do
  # Faça a solicitação à API
  # Define a URL da requisição
  url="https://api-redemet.decea.mil.br/mensagens/metar/$localidades?api_key=$api_key&data_ini=$data_ini&data_fim=$data_fim&page=$page"

  if [ $page -eq 1 ]; then
    echo "Fazendo a primeira requisição"
  fi

  if [ $page -gt 1 ]; then
    echo "Fazendo a requisição da página: $page"
  fi

  # Faz a requisição e armazena o resultado em uma variável
  response=$(curl -s "$url")

  # Verifique se a solicitação foi bem-sucedida
  status=$(echo "$response" | jq -r '.status')

  # Continua caso a requisição tenha sido feita com sucesso  
  if [ "$status" = true ]; then

    # Verificar os valores de "last_page" e "next_page_url"
    last_page=$(echo "$response" | jq '.data.last_page')
    current_page=$(echo "$response" | jq '.data.current_page')
    next_page_url=$(echo "$response" | jq '.data.next_page_url')

    if [ $page -eq 1 ]; then
      echo "Sua solicitação terá um total de $last_page página(s)"
    fi

    # Descobrindo a quantidade de registros obtidos na resposta
    tamanho=$(echo "$response" | jq -r  '.data.data | length')

    for ((i=0; i<$tamanho; i++)); do
      #     mensagem=$(echo "$response" | jq -r --arg index $i '.data.data[$index] | [.validade_inicial,.mens] | @sh' | tr -d "'")
      validade_inicial=$(echo "$response" | jq -r ".data.data[$i].validade_inicial")
      mens=$(echo "$response" | jq -r ".data.data[$i].mens")
      mensagem="$validade_inicial,$mens"
      all_mensagens+="$mensagem\n"
    done

    # Verificar se há mais páginas a serem buscadas
    if [ "$next_page_url" = "null" ] || [ "$last_page" -eq "$page" ]; then
      break
    fi

    # Incrementar o número da página
    page=$((page + 1))
  else
    # Exiba uma mensagem de erro
    message=$(echo "$response" | jq -r '.message')
    echo "Erro na solicitação: $message"
    exit 1
  fi
done

# Nome do Arquivo com o resultado
nome="msg_$(date +%Y%m%d_%H%M%S).txt"

# Obtendo a quantidade de mensagens constantes no resultado
quantidade_mensagens=$(echo -e "$all_mensagens" | wc -l)

# Salva o resultado em um arquivo de texto
echo -e "$all_mensagens" | sort > "$nome"

# Fim da contagem de tempo de execução
end_time=$(($SECONDS - $tempo_inicial))
if ((end_time < 60)); then
    duracao="${end_time} segundos"
else 
    minutes=$((end_time / 60))
    duracao="${minutes} minutos"
fi

echo "Sua requisição durou $duracao."
echo "Foram obtidas $quantidade_mensagens mensagens"
echo "O resultado foi armazenado no arquivo: $nome"