# busca_dados_api_redemet

## Name

Scripts para obtenção de dados meteorológicos à partir da API da REDEMET

## Description
Este projeto tem o objetivo de auxiliar aqueles que necessitam obter dados meteorológicos da API da REDEMET mas não possuem conhecimento de programação para criar uma rotina de download desses dados.

## Instalação

Atualmente, o script foi disponbilizado em 4 linguagens, PHP, PYTHON, BAT (Windows) e SHELLSCRIPT.

A execução de cada um deles e os requisitos da linguagem dependerão do sistema operacional utilizado.


## Uso

A documentação da API da REDEMET e a forma de obtenção da chave de acesso encontra-se no link abaixo.

https://ajuda.decea.mil.br/base-de-conhecimento/api-redemet-o-que-e/

Como esses primeiros scripts buscam apenas METAR, segue a documentação desse produto específico:

https://ajuda.decea.mil.br/base-de-conhecimento/api-redemet-mensagem-metar/

As variáveis de configuração e credenciais deverão ser alteradas dentro do próprio arquivo.

A execução do script é simples, bastante acionar o interpretador correto.

Ao final da execução, o resultado esperado é um arquivo de texto contendo as informações desejadas.

- php busca_metar_api_redemet_php.php
- python3 busca_metar_api_redemet_python.py
- bash busca_metar_api_redemet_shellscript.sh
- busca_metar_api_redemet_windows.bat

## Suporte
Não há suporte ou garantia, apenas a disposição em ajudar a comunidade.

## Roadmap
Esse primeiro script tem o foco na obtenção de METAR / SPECI mas é possível que sejam desenvolvidos mais scripts para outros tipos de produtos.

## Contribuição
Quem sabe?!

## Autor
- Fernando Reis

## Licença
For open source projects, say how it is licensed.

## Status do Projeto
O projeto foi iniciado agora, mas ainda tem muito potencial.